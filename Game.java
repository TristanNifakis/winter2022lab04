public class Game {
		private String title;
		private String genre;
		private String console;
		private double price;
		//getters
		public String getTitle(){
			return this.title;
		}
		public String getGenre(){
			return this.genre;
		}
		public String getConsole(){
			return this.console;
		}
		public double getPrice(){
			return this.price;
		}
		//setters
		public void setTitle(String title){
			this.title = title;
		}
		public void setGenre(String genre){
			this.genre = genre;
		}
		/* public void setConsole(String console){
			this.console = console;
		} */
		public void setPrice(double price){
			this.price = price;
		}
			
		public void gaming(){
			System.out.println("Sike! You still got to pay taxes, hence it cost $"+this.price*1.15);
		}
		//constructor
		public Game(String title,String genre, String console, double price){
			this.title = title;
			this.genre = genre;
			this.console = console;
			this.price = price;
		}
		
}
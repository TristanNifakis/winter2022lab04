import java.util.Scanner;
public class Shop {
	public static void main(String[]args){
	Scanner reader = new Scanner(System.in);
	Game[] collectionOfGames = new Game[4];
	
	
	
		for(int i=0;i<collectionOfGames.length;i++){
		
			System.out.println("What is the name of the game?");
			String title = reader.next();
			//collectionOfGames[i].setTitle(reader.next());
			
			System.out.println("What genre is it?");
			String genre = reader.next();
			//collectionOfGames[i].setGenre(reader.next());
			
			System.out.println("What console would you like it in?");
			String console = reader.next();
			//collectionOfGames[i].setConsole(reader.next());
			
			System.out.println("Congradulations you can choose the price of the game!");
			int price = reader.nextInt();
			//collectionOfGames[i].setPrice(reader.nextInt());
			
			collectionOfGames[i]=new Game(title, genre, console, price);
		}
		//before
		System.out.println(collectionOfGames[3].getTitle());
		System.out.println(collectionOfGames[3].getGenre());
		System.out.println(collectionOfGames[3].getConsole());
		System.out.println(collectionOfGames[3].getPrice());
		System.out.println("Update the title.");
		//reprompt
		System.out.println("What is the name of the game?");
		collectionOfGames[3].setTitle(reader.next());
		System.out.println("What genre is it?");
		collectionOfGames[3].setGenre(reader.next());
		System.out.println("Congradulations you can choose the price of the game!");
		collectionOfGames[3].setPrice(reader.nextInt());
		//after
		System.out.println(collectionOfGames[3].getTitle());
		System.out.println(collectionOfGames[3].getGenre());
		System.out.println(collectionOfGames[3].getConsole());
		System.out.println(collectionOfGames[3].getPrice());
		
		collectionOfGames[3].gaming();
	}
}